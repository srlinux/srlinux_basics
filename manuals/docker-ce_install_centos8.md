# Docker-CE Installation procedure [Centos-8]
- download Docker repository file to /etc/yum.repos.d/docker-ce.repo
    ```
    curl  https://download.docker.com/linux/centos/docker-ce.repo -o /etc/yum.repos.d/docker-ce.repo
    ```
- update RPM index cache
    ```
    yum makecache 
    ```
- install Docker CE 
    ```
    dnf -y  install docker-ce --nobest
    ```
- Start and enable Docker Service to start at boot
    ```
    systemctl enable --now docker
    ```
- docker service status should indicate running
    ```
    systemctl status  docker
    ```
- The docker group is created, but no users are added to the group. Add your user to this group to run docker commands without sudo.
    ```
    usermod -aG docker $USER
    id $USER
    ```
    - Example
        ```
        useradd docker -g docker
        id docker
        ```
- [Optional] Proxy configuration (inspired by https://docs.docker.com/config/daemon/systemd/#httphttps-proxy)
    ```bash
    mkdir -p /etc/systemd/system/docker.service.d
    # Create "http-proxy.conf" file and add proxy configuration with proper IP addresses 
    more /etc/systemd/system/docker.service.d/http-proxy.conf

    [Service]
    Environment="HTTP_PROXY=http://192.168.1.1:8080/" "HTTPS_PROXY=http://192.168.1.1:8080/" "NO_PROXY=localhost,127.0.0.1,docker-registry.example.com,.corp"

    systemctl daemon-reload
    systemctl restart docker
    systemctl show --property=Environment docker
    ```

# Verification

- Download test image and create a Container
    ```bash
    [root@austria ~]# docker pull hello-world
        Using default tag: latest
        latest: Pulling from library/hello-world
        1b930d010525: Pull complete
        Digest: sha256:c3b4ada4687bbaa170745b3e4dd8ac3f194ca95b2d0518b417fb47e5879d9b5f
        Status: Downloaded newer image for hello-world:latest

    [root@austria ~]# docker run hello-world

        Hello from Docker!
        This message shows that your installation appears to be working correctly.

        To generate this message, Docker took the following steps:
        1. The Docker client contacted the Docker daemon.
        2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
            (amd64)
        3. The Docker daemon created a new container from that image which runs the
            executable that produces the output you are currently reading.
        4. The Docker daemon streamed that output to the Docker client, which sent it
            to your terminal.

        To try something more ambitious, you can run an Ubuntu container with:
        $ docker run -it ubuntu bash

        Share images, automate workflows, and more with a free Docker ID:
        https://hub.docker.com/

        For more examples and ideas, visit:
        https://docs.docker.com/get-started/
    ```
