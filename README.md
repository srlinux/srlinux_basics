# Basic Nokia SRLinux demo
## Run a standalone SRLinux container.

0. Requirements
    - OS environment (min. system requirements):
        - Centos 8.x or Centos 7.x system with min. Kernel 4.17
        - 8 GB RAM
        - 8 core CPU
    - Software
        - Docker CE installed, minimum version 18.09 
            - [Centos-8 example](manuals/docker-ce_install_centos8.md)
        - Python version 3.6 or higher
        - license key file available 
        - SRLinux container image available
1. Copy the SRLinux inmage X.Y.Z-N.tar.xz file into Project directory on Centos 8 Host Machine.
    - Out of scope
2. Copy the license.key into Project directory.
    - Out of scope
3. Load the docker image. 
    - To load the image, the user must have root privilege, or be part of the docker group.
        ```bash
        docker image load -i 20.6.2-332.tar.xz
        ```
4. Check that the docker image was imported correctly:
    ```bash
    docker images
    REPOSITORY                     TAG                 IMAGE ID            CREATED             SIZE
    srlinux                        20.6.2-332          b83528246e92        5 weeks ago         1.32GB
    ```
5. Deploy SRLinux container using docker commands
    - The following shell commands could be executed to deploy a single SRLinux instance 
    ```sh
    srl_name="srl_dut_1"
    srl_license="$(pwd)/license.key"
    srl_config="$(pwd)/cfg/$srl_name"
    srl_version="20.6.2-332"

	docker run -t -d --rm --privileged \
	  --sysctl net.ipv6.conf.all.disable_ipv6=0 \
	  --sysctl net.ipv4.ip_forward=0 \
	  --sysctl net.ipv6.conf.all.accept_dad=0 \
	  --sysctl net.ipv6.conf.default.accept_dad=0 \
	  --sysctl net.ipv6.conf.all.autoconf=0 \
	  --sysctl net.ipv6.conf.default.autoconf=0 \
	  -u $(id -u):$(id -g) \
	  -v $srl_license:/opt/srlinux/etc/license.key:ro \
	  -v $srl_config:/etc/opt/srlinux/:rw \
	  --name $srl_name srlinux:$srl_version \
	  bash -c 'sudo opt/srlinux/bin/sr_linux'
    ```
    - Variables
        - srl_name:     [mandatory] name of the docker container (must be unique)
        - srl_license:  [mandatory] path to a valid license file (be sure, that docker process has a right access permissions)
        - srl_version:  [mandatory] SRLinux version (could be obtained from 'docker images')
        - srl_config:   [optional] path to a local directory for persistent configuration storage (be sure, that docker process has a right access permissions)
6. Deploy a SRLinux container using BASH script
    - Run [./01.start_srl.sh \<name\> [\<name\>...]](01.start_srl.sh) script
    - 1 mandatory and many optional arguments are expected
        - Each argument represents a SRLinux container name
        - If multiple arguments provided, multiple containers are created
    > Note: Each container gets its own dedicated directory under './cfg/\<name\>' subdirectory to preserve configuration files
    - SR Linux version and path to the license file are defined in env.var file
        ```sh
        srl_license="$(pwd)/license.key"
        srl_version='20.6.2-332'
        ```
    - Example:
        - ```./01.start_srl.sh srl_dut_1 srl_dut_2```
        - As a result of this command 2 docker containers are created
        ```
        CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS               NAMES
        a5633ae55dae        srlinux:20.6.2-332   "/tini -- fixuid -q …"   36 minutes ago      Up 36 minutes                           srl_dut_2
        f475e3884f28        srlinux:20.6.2-332   "/tini -- fixuid -q …"   36 minutes ago      Up 36 minutes                           srl_dut_1
        ```
        <details>
        <summary>Click to see Directory structure</summary>    

        ```bash
        $ tree cfg
        cfg
        ├── srl_dut_1
        │   ├── banner
        │   ├── cli
        │   │   └── plugins
        │   ├── config.json
        │   ├── devices
        │   │   ├── hw_details.json
        │   │   ├── management-ip.json
        │   │   ├── slot_16.json
        │   │   ├── slot_1.json
        │   │   └── slot_nums.json
        │   ├── tls
        │   └── ztp
        └── srl_dut_2
            ├── banner
            ├── cli
            │   └── plugins
            ├── config.json
            ├── devices
            │   ├── hw_details.json
            │   ├── management-ip.json
            │   ├── slot_16.json
            │   ├── slot_1.json
            │   └── slot_nums.json
            ├── tls
            └── ztp
        ```
7. Turn off the Docker0 Tx checksum offload.
    - By default SRL container uses 'Doceker0' bridge for the visrtual management port (mgmt0). At the moment of writing, offload functionality has to be disabled for this interface (which doesn't have any value anyway for the vistual interface)
        ```
        ethtool --offload docker0 tx off
        ```
8. Access SRL CLI. Default username/password: **admin/admin**
    - via 'Console'
        ```
        docker exec -ti srl_dut_1 sr_cli
        ```
    - via SSH
        ```
        ssh admin@srl_dut_1
        ```
## Add a virtual link between containers
0. Topology
    
    ![](manuals/images/topology.png)
1. Create virtula link between containers manually
    ```sh
    srl_a='srl_dut_1'
    srl_a_int='e1-1'
    srl_b='srl_dut_2'
    srl_b_int='e1-1'
    
    # Create VETH connection between $srl_a and $srl_b containers
        ip link add $srl_a_int netns $srl_a type veth peer name $srl_b_int netns $srl_b
        ip netns exec $srl_a ip link set $srl_a_int up
        ip netns exec $srl_b ip link set $srl_b_int up

    #Disable Offload option for both container
        docker exec -ti $srl_a ethtool --offload $srl_a_int rx off tx off
        docker exec -ti $srl_b ethtool --offload $srl_b_int rx off tx off
    ```

2. Create a virtual link between 2 running containers using BASH script
    - Run [./02.create_virtual_link.sh \<arg-1\> \<arg-2\>](02.create_virtual_link.sh) script
    - 2 mandatory arguments are expected
        - Each argument has a special format: 'container_name:interface_name'. Example: **srl_dut_a:e1-2**
        - Interface name has a strict format requirement
    - Example:
        - ```./02.create_virtual_link.sh srl_dut_1:e1-1 srl_dut_2:e1-1```
        - As a result of this command, container 'srl_dut_1' port 'ethernet-1/1' is connected to container 'srl_dut_2' port 'ethernet-1/1'

## Destroy containers
1. Destroy containers
    - Run [./99.destroy.sh](99.destroy.sh) script
    - No arguments
    - Script is interactive and requests for a delete confirmation per Container

## Configuration
- Visit a dedicated GitLab project for the configuration examples
    - [https://gitlab.com/srlinux/srlinux_cli_examples](https://gitlab.com/srlinux/srlinux_cli_examples)
